# frozen_string_literal: true

class SessionsController < ApplicationController
  def new; end

  def create
    user = authenticate(params[:pseudonym], params[:password])
    if user
      session[:user_id] = user.id
      redirect_to root_url, notice: 'Logged in!'
    else
      flash.now.alert = 'Invalid pseudonym or password'
      render :new
    end
  end

  # :reek:UtilityFunction
  def authenticate(pseudonym, password)
    user = User.find_by(pseudonym: pseudonym)
    user if user && user.passhash == BCrypt::Engine.hash_secret(password, user.passsalt)
  end

  def destroy
    session[:user_id] = nil
    redirect_to root_url, notice: 'Logged out!'
  end
end
