# frozen_string_literal: true

# :reek:InstanceVariableAssumption
class UsersController < ApplicationController
  include ApplicationHelper

  before_action :set_user, only: %i[show edit update destroy]

  # GET /users
  def index
    page_num = params[:page]
    @users = User.where(deleted_at: nil).page page_num
    @users_archive = User.where(:deleted_at.ne => nil).page page_num
  end

  # GET /users/:pseudonym
  def show; end

  # GET /users/new
  def new
    @user = User.new
  end

  # GET /users/:pseudonym/edit
  def edit; end

  # POST /users
  def create
    @user = User.new(create_params)

    if @user.save
      # redirect_to root_url, :notice => "Signed up!"
      redirect_to users_path, notice: 'User was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /users/:pseudonym
  def update
    return unless access_by_user_id(@user._id)

    if @user.update(user_params)
      redirect_to users_path, notice: 'User was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /users/:pseudonym
  def destroy
    return unless access_by_user_id(@user._id)

    @user.destroy
    redirect_to users_url, notice: 'User was successfully destroyed.'
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_user
    @user = user(params[:pseudonym])
  end

  # Only allow a trusted parameter "white list" through.
  def user_params
    # TODO: make separate for each endpoint
    params.require(:user).permit(:pseudonym, :name, :email, :company_id, :password)
  end

  def create_params
    params.require(:user).permit(:pseudonym, :name, :email, :company_id, :password, :password_confirmation)
  end
end
