# frozen_string_literal: true

module ApplicationHelper
  protected

  def company_by_id(id)
    Company.find(id)
  end

  def post_by_id(id)
    Post.find(id)
  end

  def user_by_id(id)
    User.find(id)
  end

  def company(tag)
    Company.find_by(tag: tag)
  end

  def post(slug)
    Post.find_by(slug: slug)
  end

  def user(pseudonym)
    User.find_by(pseudonym: pseudonym)
  end

  def access_by_user_id(valid_id=nil)
    return true if valid_id.blank? && current_user.pseudonym == ENV.fetch('ADMIN_USER') { 'admin' }
    return unless current_user

    current_user._id == valid_id
  end
end
