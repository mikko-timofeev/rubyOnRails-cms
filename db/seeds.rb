# frozen_string_literal: true

[Company, User, Post].each do |table|
  table.all.delete_all
end

c1 = Company.create!(
  tag: 'freelance',
  title: 'Фриланс'
)

c2 = Company.create!(
  tag: 'evilcorp',
  title: 'Evil Corp'
)

User.create!(
  pseudonym: 'admin',
  email: 'ivan@mail.ru',
  company_id: c1._id,
  name: 'Иванов Иван Иванович',
  password: ENV.fetch('ADMIN_PASSWORD') { 'password' },
  password_confirmation: ENV.fetch('ADMIN_PASSWORD') { 'password' }
)

u2 = User.create!(
  pseudonym: 'admin1',
  email: 'ivan1@mail.ru',
  company_id: c2._id,
  name: 'Петров Пётр Петрович',
  password: 'password',
  password_confirmation: 'password'
)

Post.create!(
  slug: 'readme',
  company_id: c1._id,
  user_id: u2._id,
  title: 'Приветствие',
  text: 'Добро пожаловать! Это главная страница.',
  keyword: ''
)

Post.create!(
  slug: 'hiddenpost',
  company_id: c2._id,
  user_id: u2._id,
  title: 'Приветствие',
  text: 'Вы читаете этот текст, если ввели кодовое слово, вошли как сотрудник компании или напрямую — в файле сидов',
  keyword: 'evil'
)
