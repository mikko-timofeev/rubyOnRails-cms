# frozen_string_literal: true

FactoryBot.define do
  factory :company do
    title { Faker::String.random(length: 3..30) }
  end
end
