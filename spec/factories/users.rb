# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    name { Faker::String.random(length: 3..30) }
    email { Faker::Internet.free_email }
    encrypted_password { Faker::Internet.password(min_length: 10, max_length: 20) }
    salt { Faker::String.random(length: 3..30) }
  end
end
