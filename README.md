# README

## Setup process
```
cp .env.sample .env
<edit .env file if necessary>
docker-compose build
docker-compose run --rm web yarn install
docker-compose run --rm web bundle exec rails runner db/seeds.rb
docker-compose up
```

## Troubleshooting db access
```
chown -R 1001 tmp/db
```

## Tests and linters
```
docker-compose run --rm web bundle exec rubocop
docker-compose run --rm web bundle exec reek
docker-compose run --rm web bundle exec brakeman -z -q
docker-compose run --rm web bundle exec haml-lint app/views/
```

---

# TODO:
* Upgrade bundle
* Add fasterer gem
* Bug fixes
* More validations
* SPECs
* Fix Rake tasks
* Code deduplication
* Optimisation

## To be continued...
```
docker-compose run --rm web bundle exec rspec
docker-compose run --rm web yarn run lint
```
